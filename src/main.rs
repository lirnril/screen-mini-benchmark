#![feature(test)]

extern crate test;

mod vecmath {
    pub type Vector3<T> = [T; 3];
}

pub fn screen1(
    op1: &Vec<vecmath::Vector3<f64>>,
    op2: &mut Vec<vecmath::Vector3<f64>>,
) {
    for i in 0..op1.len() {
        op2[i][0] = (1.0 - (1.0 - op1[i][0]) * (1.0 - op2[i][0])).clamp(0.0, 1.0);
        op2[i][1] = (1.0 - (1.0 - op1[i][1]) * (1.0 - op2[i][1])).clamp(0.0, 1.0);
        op2[i][2] = (1.0 - (1.0 - op1[i][2]) * (1.0 - op2[i][2])).clamp(0.0, 1.0);
    }
}

pub fn screen2(
    op1: &Vec<vecmath::Vector3<f64>>,
    op2: &mut Vec<vecmath::Vector3<f64>>,
) {
    fn blend(a: f64, b: f64) -> f64 {
        (1.0 - (1.0 - a) * (1.0 - b)).clamp(0.0, 1.0)
    }
    for i in 0..op1.len() {
        op2[i][0] = blend(op1[i][0], op2[i][0]);
        op2[i][1] = blend(op1[i][1], op2[i][1]);
        op2[i][2] = blend(op1[i][2], op2[i][2]);
    }
}

pub fn screen3(
    op1: &Vec<vecmath::Vector3<f64>>,
    op2: &mut Vec<vecmath::Vector3<f64>>,
) {
    fn blend(v1: f64, v2: f64) -> f64 {
        (1.0 - (1.0 - v1) * (1.0 - v2)).clamp(0.0, 1.0)
    }
    for (r1, r2) in op1.iter().zip(op2) {
        r2[0] = blend(r1[0], r2[0]);
        r2[1] = blend(r1[1], r2[1]);
        r2[2] = blend(r1[2], r2[2]);
    }
}

pub fn screen4(
    op1: &Vec<vecmath::Vector3<f64>>,
    op2: &mut Vec<vecmath::Vector3<f64>>,
) {
    fn blend(v1: f64, v2: f64) -> f64 {
        (1.0 - (1.0 - v1) * (1.0 - v2)).clamp(0.0, 1.0)
    }
    for (r1, r2) in op1.iter().zip(op2) {
        for (v1, v2) in r1.iter().zip(r2) {
            *v2 = blend(*v1, *v2);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    macro_rules! go {
        ($b:expr, $func:expr) => {
            let op1 = vec![[0.5; 3]; 1024];
            let mut op2 = vec![[0.7; 3]; 1024];
            $b.iter(|| {
                $func(&op1, &mut op2);
            })
        }
    }

    #[bench] fn bench_screen1(b: &mut Bencher) { go!(b, screen1); }
    #[bench] fn bench_screen2(b: &mut Bencher) { go!(b, screen2); }
    #[bench] fn bench_screen3(b: &mut Bencher) { go!(b, screen3); }
    #[bench] fn bench_screen4(b: &mut Bencher) { go!(b, screen4); }
}
